import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import styled, { keyframes } from 'styled-components';

import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Panel from 'react-bootstrap/lib/Panel';
import UnstyledAccordion from 'react-bootstrap/lib/Accordion';
import Form from 'react-bootstrap/lib/Form';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import InputGroup from 'react-bootstrap/lib/InputGroup';
import FormControl from 'react-bootstrap/lib/FormControl';
import Button from 'react-bootstrap/lib/Button';
import UnstyledGlyphicon from 'react-bootstrap/lib/Glyphicon';

import {
  FieldSelectGroup,
  userCan,
} from 'utils/helpers';

import {
  getSexOptions,
  getInterlocutorLocationOptions,
  getSearchTimeOptions,
} from 'utils/options';

import agentmessages from 'components/AgentView/messages';

const Accordion = styled(UnstyledAccordion)`
  & .panel {
    background: transparent;
    border: none;
    box-shadow: none;
  }
  
  & .panel-heading {
    background: transparent;
    border: none;
    padding: 0;
    margin-bottom: 1rem;
  }
  
  & .panel-heading .panel-title {
    font-size: smaller;
    text-align: right;
  }
  
  & .panel-body {
    border: 1px solid #ddd;
    border-radius: 4px;
  }
`;

const kfSpin = keyframes`
    from { transform: scale(1) rotate(0deg);}
    to { transform: scale(1) rotate(360deg);}
`;

const kfSpin2 = keyframes`
    from { -webkit-transform: rotate(0deg);}
    to { -webkit-transform: rotate(360deg);}
`;

const Glyphicon = styled(UnstyledGlyphicon)`
  &.glyphicon.spinning {
    animation: ${kfSpin} 1s infinite linear;
    -webkit-animation: ${kfSpin2} 1s infinite linear;
  }
`;

export const CaseListFilter = ({ currentlySyncing, search, doSearch, updateSearchId, updateSearchName, updateSearchSex, updateSearchPostcode, updateSearchTime, updateSearchCentreName, updateSearchCallerLocation, updateSearchCallerOrganisation, updateSearchFilter, userSPI, intl, ...props }) => (
  <Form className={props.className} >
    <FormGroup>
      <InputGroup>
        <FormControl
          componentClass="select"
          placeholder="Show"
          value={search.filter}
          onChange={(e) => updateSearchFilter(e.target.value)}
        >
          <option value="all">All</option>
          <option value="open">Open</option>
          <option value="pending">Closed Pending Review</option>
          <option value="closed">Closed</option>
          {userCan(userSPI, 'view deleted files') ? <option value="deleted">Deleted</option> : null}
        </FormControl>
        <InputGroup.Button>
          <Button
            onClick={() => doSearch(search)}
            disabled={currentlySyncing}
          >
            <Glyphicon glyph={'refresh'} className={currentlySyncing ? 'spinning' : ''} />
          </Button>
        </InputGroup.Button>
      </InputGroup>
    </FormGroup>
    <Accordion id={'caseListSearch'}>
      <Panel eventKey="1">
        <Panel.Heading>
          <Panel.Title toggle>Advanced Search</Panel.Title>
        </Panel.Heading>
        <Panel.Body collapsible>
          <Row style={{ marginBottom: '1rem' }}>
            <Col xs={12}>
              <FormControl
                type="text"
                placeholder="ID"
                value={search.naturalId}
                onChange={(e) => updateSearchId(e.target.value)}
              />
            </Col>
          </Row>
          <Row>
            <Col xs={8}>
              <FormControl
                type="text"
                placeholder="Patient Name"
                value={search.name}
                onChange={(e) => updateSearchName(e.target.value)}
              />
            </Col>
            <Col xs={4}>
              <FieldSelectGroup
                id="modalSex"
                placeholder="Gender"
                options={getSexOptions()}
                value={search.sex}
                onChange={(e) => updateSearchSex(e.target.value)}
              />
            </Col>
          </Row>
          <Row style={{ marginBottom: '1rem' }}>
            <Col xs={6}>
              <FormControl
                type="text"
                placeholder="Postcode"
                value={search.postcode}
                onChange={(e) => updateSearchPostcode(e.target.value)}
              />
            </Col>
            <Col xs={6}>
              <FieldSelectGroup
                id="modalTime"
                placeholder="Time of Call"
                options={getSearchTimeOptions()}
                value={search.time}
                onChange={(e) => updateSearchTime(e.target.value)}
              />
            </Col>
          </Row>
          <Row>
            <Col xs={6}>
              <FormControl
                type="text"
                placeholder={intl.formatMessage(agentmessages['field.centreAgent.name'])}
                value={search.centreName}
                onChange={(e) => updateSearchCentreName(e.target.value)}
              />
            </Col>
            <Col xs={6}>
              <FieldSelectGroup
                id="modalLocation"
                placeholder="Caller Location"
                options={getInterlocutorLocationOptions()}
                value={search.callerLocation}
                onChange={(e) => updateSearchCallerLocation(e.target.value)}
              />
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <FormControl
                type="text"
                placeholder="Caller Organisation"
                value={search.callerOrganisation}
                onChange={(e) => updateSearchCallerOrganisation(e.target.value)}
              />
            </Col>
          </Row>
        </Panel.Body>
      </Panel>
    </Accordion>
  </Form>
);


CaseListFilter.propTypes = {
  className: PropTypes.string,
  currentlySyncing: PropTypes.bool,
  doSearch: PropTypes.func,
  intl: PropTypes.object,
  search: PropTypes.object,
  updateSearchCallerLocation: PropTypes.func,
  updateSearchCallerOrganisation: PropTypes.func,
  updateSearchCentreName: PropTypes.func,
  updateSearchFilter: PropTypes.func,
  updateSearchId: PropTypes.func,
  updateSearchName: PropTypes.func,
  updateSearchPostcode: PropTypes.func,
  updateSearchSex: PropTypes.func,
  updateSearchTime: PropTypes.func,
  userSPI: PropTypes.object,
};

export default injectIntl(styled(CaseListFilter)`
  ${({ theme }) => theme.CaseListFilter}
`);
