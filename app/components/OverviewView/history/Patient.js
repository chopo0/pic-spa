import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import casemessages from 'components/CaseView/messages';
import messages from 'components/PatientView/messages';

const PatientHistory = ({ history }) => <div>
  <br /><strong><FormattedMessage {...casemessages['section.patient']} /></strong>
  <br />
  { (history.topic)
      ? <span>
        <strong><FormattedMessage {...messages['field.topic']} />:</strong> {history.topic[1]} {(history.topic[0]) ? <span>(was {history.topic[0]})</span> : null}
        <br />
      </span>
      : null
  }
  {
    (
        history.name
        || history.medicalRecordNumber
        || history.animalType
        || history.gender
        || history.location
        || history.postcode
    )
        ? <div>
          { (history.name)
              ? <span>
                <strong><FormattedMessage {...messages['field.name']} />:</strong> {history.name[1]} {(history.name[0]) ? <span>(was {history.name[0]})</span> : null}
              </span>
              : null
          }
          { (history.medicalRecordNumber)
              ? <span>
                <strong><FormattedMessage {...messages['field.mrn']} />:</strong> {history.medicalRecordNumber[1]} {(history.medicalRecordNumber[0]) ? <span>(was {history.medicalRecordNumber[0]})</span> : null}
              </span>
              : null
          }
          { (history.animalType)
              ? <span>
                <strong><FormattedMessage {...messages['field.animalType']} />:</strong> {history.animalType[1]} {(history.animalType[0]) ? <span>(was {history.animalType[0]})</span> : null}
              </span>
              : null
          }
          { (history.gender)
              ? <span>
                <strong><FormattedMessage {...messages['field.sex']} />:</strong> {history.gender[1]} {(history.gender[0]) ? <span>(was {history.gender[0]})</span> : null}
              </span>
              : null
          }
          { (history.location)
              ? <span>
                <strong><FormattedMessage {...messages['field.location']} />:</strong> {history.location[1]} {(history.location[0]) ? <span>(was {history.location[0]})</span> : null}
              </span>
              : null
          }
          { (history.postcode)
              ? <span>
                <strong><FormattedMessage {...messages['field.postcode']} />:</strong> {history.postcode[1]} {(history.postcode[0]) ? <span>(was {history.postcode[0]})</span> : null}
              </span>
              : null
          }
        </div>
        : null
  }
  {
    (
        history.circumstance
        || history.occupation
    )
        ? <div>
          { (history.circumstance)
              ? <span>
                <strong><FormattedMessage {...messages['field.circumstance']} />:</strong> {history.circumstance[1]} {(history.circumstance[0]) ? <span>(was {history.circumstance[0]})</span> : null}
      &nbsp;
              </span>
              : null
          }
          { (history.occupation)
              ? <span>
                <strong><FormattedMessage {...messages['field.occupation']} />:</strong> {history.occupation[1]} {(history.occupation[0]) ? <span>(was {history.occupation[0]})</span> : null}
              </span>
              : null
          }
        </div>
        : null
  }
  {
    (
        history.weight
        || history.weightUnits
        || history.age
        || history.ageGroup
        || history.ageYears
    )
        ? <div>
          { (
              history.weight
              || history.weightUnits
          )
              ? <span>
                <strong><FormattedMessage {...messages['field.weight']} />:</strong> { (history.weight) ? history.weight[1] : null}
                { (history.weightUnits) ? history.weightUnits[1] : null}&nbsp;
                {(
                    (history.weight && history.weight[0] !== history.weight[1])
                    || (history.weightUnits && history.weightUnits[0] !== history.weightUnits[1])
                )
                    ? <span>
                      (was {(history.weight) ? history.weight[0] : null}
                      {(history.weightUnits) ? history.weightUnits[0] : null})
                    </span>
          : null}
      &nbsp;
              </span>
              : null
          }
          { (history.age)
              ? <span>
                <strong><FormattedMessage {...messages['field.age']} />:</strong> {history.age[1]} {(history.age[0]) ? <span>(was {history.age[0]})</span> : null}
              </span>
              : null
          }
          { (history.ageGroup)
              ? <span>
                <strong><FormattedMessage {...messages['field.ageGroup']} />:</strong> {history.ageGroup[1]} {(history.ageGroup[0]) ? <span>(was {history.ageGroup[0]})</span> : null}
              </span>
              : null
          }
          { (history.ageYears)
              ? <span>
                <strong><FormattedMessage {...messages['field.ageYears']} />:</strong> {history.ageYears[1]} {(history.ageYears[0]) ? <span>(was {history.ageYears[0]})</span> : null}
              </span>
              : null
          }
        </div>
        : null
  }
  { (history.poisonSeverityScore)
      ? <div>
        <strong><FormattedMessage {...messages['field.poisonseverityscore']} />:</strong> {history.poisonSeverityScore[1]} {(history.poisonSeverityScore[0]) ? <span>(was {history.poisonSeverityScore[0]})</span> : null}
      </div>
      : null
  }
  { (history.riskAssessment)
      ? <div>
        <strong><FormattedMessage {...messages['field.riskassessment']} />:</strong> {history.riskAssessment[1]} {(history.riskAssessment[0]) ? <span>(was {history.riskAssessment[0]})</span> : null}
      </div>
      : null
  }
</div>;

PatientHistory.propTypes = {
  history: PropTypes.object,
};

export default PatientHistory;
