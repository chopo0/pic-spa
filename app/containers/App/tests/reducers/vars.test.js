/* eslint-disable */

import varsReducer from '../../reducers/vars';

import {
  setAgentTab
} from 'containers/AgentEntry/actions/case';

import {
  addCallRecord,
  addCase,
  endCall,
  resetAgentViewCentreName,
  updateAgentViewCentreName,
} from 'containers/CaseEntry/actions/case';

import {
  changeForm,
  clearError,
  displayRecord,
  requestError,
  sendingRequest,
  setAuthState,
  setTab,
  setUiModeDefault,
  setUiModeEditTemplate,
  swUpdated,
  updateToxicologySource,
  syncingPouch,
} from '../../actions';

describe('varsReducer', () => {
  it('returns the initial state', () => {
    expect(varsReducer(undefined, {})).toMatchSnapshot();
  });
  it('add call record', () => {
    expect(varsReducer({}, addCallRecord(1, { 'name': 'Test SPI' }))).toMatchSnapshot();
  });
  it('add case', () => {
    const action = addCase({ 'name': 'Test SPI' });
    action.id = 'b72540f9163ef35a05213611e012f2bdf8ccefb3';
    expect(varsReducer({}, action)).toMatchSnapshot();
  });
  it('change form', () => {
    expect(varsReducer({}, changeForm({'username': 'New form user', 'password': 'New form password'}))).toMatchSnapshot();
  });
  it('clear error', () => {
    expect(varsReducer({}, clearError())).toMatchSnapshot();
  });
  it('display record', () => {
    expect(varsReducer({}, displayRecord(1))).toMatchSnapshot();
  });
  it('end call', () => {
    expect(varsReducer({}, endCall())).toMatchSnapshot();
  });
  it('request error', () => {
    expect(varsReducer({}, requestError({'name': 'Test error'}))).toMatchSnapshot();
  });
  it('reset agent view centre name', () => {
    expect(varsReducer({}, resetAgentViewCentreName())).toMatchSnapshot();
  });
  it('sending request', () => {
    expect(varsReducer({}, sendingRequest(false))).toMatchSnapshot();
  });
  it('set auth state', () => {
    const token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwMDAvYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MDkzNzcxODgsImV4cCI6MTUwOTM4MDc4OCwibmJmIjoxNTA5Mzc3MTg4LCJqdGkiOiJFeG5vTXkwcmRNYkxYbTJ1Iiwic3ViIjoxLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIiwibmFtZSI6IkFkbWluIiwicm9sZSI6ImFkbWluIiwib3JnIjoiV0EgUG9pc29ucyBJbmZvcm1hdGlvbiBDZW50cmUiLCJwaG9uZSI6IigwOCkgOTk5OSA5OTk5Iiwib3JnX3N0cmVldCI6Ikhvc3BpdGFsIEF2ZSIsIm9yZ19zdWJ1cmIiOiJOZWRsYW5kcyIsIm9yZ19zdGF0ZSI6IldBIiwib3JnX3Bvc3Rjb2RlIjoiNjAwOSJ9._XaDXv30uOn0yNhqBEBIIa7G_QKvqvaX6tl419EyCLc';
    expect(varsReducer({}, setAuthState(true, token))).toMatchSnapshot();
  });
  it('set agent tab', () => {
    expect(varsReducer({}, setAgentTab(2))).toMatchSnapshot();
  });
  it('set tab', () => {
    expect(varsReducer({}, setTab(1))).toMatchSnapshot();
  });
  it('set ui mode default', () => {
    expect(varsReducer({}, setUiModeDefault())).toMatchSnapshot();
  });
  it('set ui mode edit template', () => {
    expect(varsReducer({}, setUiModeEditTemplate())).toMatchSnapshot();
  });
  it('sw updated', () => {
    expect(varsReducer({}, swUpdated())).toMatchSnapshot();
  });
  it('toxicology source updated', () => {
    expect(varsReducer({}, updateToxicologySource('PoisinDex'))).toMatchSnapshot();
  });
  it('syncing pouch', () => {
    expect(varsReducer({}, syncingPouch(false))).toMatchSnapshot();
  });
  it('update agent view centre name', () => {
    expect(varsReducer({}, updateAgentViewCentreName('Test name'))).toMatchSnapshot();
  });
});
