import {
  LOAD_REPOS,
  LOAD_REPOS_SUCCESS,
  LOAD_REPOS_ERROR,
  BATCH_INSERT_DOCUMENT,
  CHANGE_FORM,
  CLEAR_ERROR,
  DELETE_DOCUMENT,
  DISPLAY_RECORD,
  EXECUTE_SEARCH,
  INSERT_DOCUMENT,
  LOGIN_REQUEST,
  LOGOUT,
  REGISTER_REQUEST,
  REQUEST_ERROR,
  SENDING_REQUEST,
  SET_AUTH,
  SET_TAB,
  SET_UI_MODE_DEFAULT,
  SET_UI_MODE_EDIT_TEMPLATES,
  SW_UPDATED,
  UPDATE_TOXICOLOGY_SOURCE,
  SYNCING_POUCH,
  UPDATE_ACTION_BAR_SEARCH_RESULTS,
  UPDATE_DOCUMENT,
  UPDATE_CASE_LIST_SEARCH_RESULTS,
  UPDATE_NEW_RECORD_SEARCH_RESULTS,
} from '../constants';

describe('App action constants', () => {
  it('LOAD_REPOS', () => {
    expect(LOAD_REPOS).toMatchSnapshot();
  });
  it('LOAD_REPOS_SUCCESS', () => {
    expect(LOAD_REPOS_SUCCESS).toMatchSnapshot();
  });
  it('LOAD_REPOS_ERROR', () => {
    expect(LOAD_REPOS_ERROR).toMatchSnapshot();
  });
  it('BATCH_INSERT_DOCUMENT', () => {
    expect(BATCH_INSERT_DOCUMENT).toMatchSnapshot();
  });
  it('CHANGE_FORM', () => {
    expect(CHANGE_FORM).toMatchSnapshot();
  });
  it('CLEAR_ERROR', () => {
    expect(CLEAR_ERROR).toMatchSnapshot();
  });
  it('DELETE_DOCUMENT', () => {
    expect(DELETE_DOCUMENT).toMatchSnapshot();
  });
  it('DISPLAY_RECORD', () => {
    expect(DISPLAY_RECORD).toMatchSnapshot();
  });
  it('EXECUTE_SEARCH', () => {
    expect(EXECUTE_SEARCH).toMatchSnapshot();
  });
  it('INSERT_DOCUMENT', () => {
    expect(INSERT_DOCUMENT).toMatchSnapshot();
  });
  it('LOGIN_REQUEST', () => {
    expect(LOGIN_REQUEST).toMatchSnapshot();
  });
  it('LOGOUT', () => {
    expect(LOGOUT).toMatchSnapshot();
  });
  it('REGISTER_REQUEST', () => {
    expect(REGISTER_REQUEST).toMatchSnapshot();
  });
  it('REQUEST_ERROR', () => {
    expect(REQUEST_ERROR).toMatchSnapshot();
  });
  it('SENDING_REQUEST', () => {
    expect(SENDING_REQUEST).toMatchSnapshot();
  });
  it('SET_AUTH', () => {
    expect(SET_AUTH).toMatchSnapshot();
  });
  it('SET_TAB', () => {
    expect(SET_TAB).toMatchSnapshot();
  });
  it('SET_UI_MODE_DEFAULT', () => {
    expect(SET_UI_MODE_DEFAULT).toMatchSnapshot();
  });
  it('SET_UI_MODE_EDIT_TEMPLATES', () => {
    expect(SET_UI_MODE_EDIT_TEMPLATES).toMatchSnapshot();
  });
  it('SW_UPDATED', () => {
    expect(SW_UPDATED).toMatchSnapshot();
  });
  it('UPDATE_TOXICOLOGY_SOURCE', () => {
    expect(UPDATE_TOXICOLOGY_SOURCE).toMatchSnapshot();
  });
  it('SYNCING_POUCH', () => {
    expect(SYNCING_POUCH).toMatchSnapshot();
  });
  it('UPDATE_ACTION_BAR_SEARCH_RESULTS', () => {
    expect(UPDATE_ACTION_BAR_SEARCH_RESULTS).toMatchSnapshot();
  });
  it('UPDATE_DOCUMENT', () => {
    expect(UPDATE_DOCUMENT).toMatchSnapshot();
  });
  it('UPDATE_CASE_LIST_SEARCH_RESULTS', () => {
    expect(UPDATE_CASE_LIST_SEARCH_RESULTS).toMatchSnapshot();
  });
  it('UPDATE_NEW_RECORD_SEARCH_RESULTS', () => {
    expect(UPDATE_NEW_RECORD_SEARCH_RESULTS).toMatchSnapshot();
  });
});
