/* eslint-disable */

import ReferralsViewReducer from '../../reducers/referralsView';

import {
  resetReferralsView,
  updateReferralsViewContent,
  updateReferralsViewReferee,
  updateReferralsViewSpecialty,
} from '../../actions';

describe('ReferralsViewReducer', () => {
  it('returns the initial state', () => {
    expect(ReferralsViewReducer(undefined, {})).toMatchSnapshot();
  });
  it('reset referrals view', () => {
    expect(ReferralsViewReducer({'content': 'Test', 'referee': 'Test', 'specialty': 'Test'}, resetReferralsView())).toMatchSnapshot();
  });
  it('update referrals view content', () => {
    expect(ReferralsViewReducer({'content': 'Test', 'referee': 'Test', 'specialty': 'Test'}, updateReferralsViewContent('Test Content'))).toMatchSnapshot();
  });
  it('update referrals view referee', () => {
    expect(ReferralsViewReducer({'content': 'Test', 'referee': 'Test', 'specialty': 'Test'}, updateReferralsViewReferee('Test Referee'))).toMatchSnapshot();
  });
  it('update referrals view specialty', () => {
    expect(ReferralsViewReducer({'content': 'Test', 'referee': 'Test', 'specialty': 'Test'}, updateReferralsViewSpecialty('Test Specialty'))).toMatchSnapshot();
  });
});
