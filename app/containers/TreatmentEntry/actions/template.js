import {
  UPDATE_TEMPLATE_REFERRALS,
  UPDATE_TEMPLATE_TREATMENTS,
} from '../constants';

export const updateTemplateReferrals = (templateId, noteId, referee, specialty) => ({
  type: UPDATE_TEMPLATE_REFERRALS,
  templateId,
  noteId,
  referee,
  specialty,
});

export const updateTemplateTreatments = (templateId, noteId, disposition, treatments, spi) => ({
  type: UPDATE_TEMPLATE_TREATMENTS,
  templateId,
  noteId,
  disposition,
  treatments,
  spi,
});
