export const UPDATE_PATIENT_AGE = 'pic/PatientEntry/UPDATE_PATIENT_AGE';
export const UPDATE_PATIENT_AGE_GROUP = 'pic/PatientEntry/UPDATE_PATIENT_AGE_GROUP';
export const UPDATE_PATIENT_AGE_YEARS = 'pic/PatientEntry/UPDATE_PATIENT_AGE_YEARS';
export const UPDATE_PATIENT_ANIMAL_TYPE = 'pic/PatientEntry/UPDATE_PATIENT_ANIMAL_TYPE';
export const UPDATE_PATIENT_CATEGORY = 'pic/PatientEntry/UPDATE_PATIENT_CATEGORY';
export const UPDATE_PATIENT_CIRCUMSTANCE = 'pic/PatientEntry/UPDATE_PATIENT_CIRCUMSTANCE';
export const UPDATE_PATIENT_LOCATION = 'pic/PatientEntry/UPDATE_PATIENT_LOCATION';
export const UPDATE_PATIENT_MEDICAL_RECORD_NUMBER = 'pic/PatientEntry/UPDATE_PATIENT_MEDICAL_RECORD_NUMBER';
export const UPDATE_PATIENT_NAME = 'pic/PatientEntry/UPDATE_PATIENT_NAME';
export const UPDATE_PATIENT_OCCUPATION = 'pic/PatientEntry/UPDATE_PATIENT_OCCUPATION';
export const UPDATE_PATIENT_POISON_SEVERITY_SCORE = 'pic/PatientEntry/UPDATE_PATIENT_POISON_SEVERITY_SCORE';
export const UPDATE_PATIENT_SUBURB = 'pic/PatientEntry/UPDATE_PATIENT_SUBURB';
export const UPDATE_PATIENT_POSTCODE = 'pic/PatientEntry/UPDATE_PATIENT_POSTCODE';
export const UPDATE_PATIENT_GEOMETRY = 'pic/PatientEntry/UPDATE_PATIENT_GEOMETRY';
export const UPDATE_PATIENT_PREGNANT_STATUS = 'pic/PatientEntry/UPDATE_PATIENT_PREGNANT_STATUS';
export const UPDATE_PATIENT_PREGNANT_TRIMESTER = 'pic/PatientEntry/UPDATE_PATIENT_PREGNANT_TRIMESTER';
export const UPDATE_PATIENT_PREGNANT_TRIMESTER_UNKNOWN = 'pic/PatientEntry/UPDATE_PATIENT_PREGNANT_TRIMESTER_UNKNOWN';
export const UPDATE_PATIENT_PREGNANT_LACTATING = 'pic/PatientEntry/UPDATE_PATIENT_PREGNANT_LACTATING';
export const UPDATE_PATIENT_RISK_ASSESSMENT = 'pic/PatientEntry/UPDATE_PATIENT_RISK_ASSESSMENT';
export const UPDATE_PATIENT_SEX = 'pic/PatientEntry/UPDATE_PATIENT_SEX';
export const UPDATE_PATIENT_WEIGHT = 'pic/PatientEntry/UPDATE_PATIENT_WEIGHT';
export const UPDATE_TEMPLATE_PATIENT_AGE = 'pic/PatientEntry/UPDATE_TEMPLATE_PATIENT_AGE';
export const UPDATE_TEMPLATE_PATIENT_AGE_GROUP = 'pic/PatientEntry/UPDATE_TEMPLATE_PATIENT_AGE_GROUP';
export const UPDATE_TEMPLATE_PATIENT_AGE_YEARS = 'pic/PatientEntry/UPDATE_TEMPLATE_PATIENT_AGE_YEARS';
export const UPDATE_TEMPLATE_PATIENT_ANIMAL_TYPE = 'pic/PatientEntry/UPDATE_TEMPLATE_PATIENT_ANIMAL_TYPE';
export const UPDATE_TEMPLATE_PATIENT_CATEGORY = 'pic/PatientEntry/UPDATE_TEMPLATE_PATIENT_CATEGORY';
export const UPDATE_TEMPLATE_PATIENT_CIRCUMSTANCE = 'pic/PatientEntry/UPDATE_TEMPLATE_PATIENT_CIRCUMSTANCE';
export const UPDATE_TEMPLATE_PATIENT_LOCATION = 'pic/PatientEntry/UPDATE_TEMPLATE_PATIENT_LOCATION';
export const UPDATE_TEMPLATE_PATIENT_MEDICAL_RECORD_NUMBER = 'pic/PatientEntry/UPDATE_TEMPLATE_PATIENT_MEDICAL_RECORD_NUMBER';
export const UPDATE_TEMPLATE_PATIENT_NAME = 'pic/PatientEntry/UPDATE_TEMPLATE_PATIENT_NAME';
export const UPDATE_TEMPLATE_PATIENT_OCCUPATION = 'pic/PatientEntry/UPDATE_TEMPLATE_PATIENT_OCCUPATION';
export const UPDATE_TEMPLATE_PATIENT_POISON_SEVERITY_SCORE = 'pic/PatientEntry/UPDATE_TEMPLATE_PATIENT_POISON_SEVERITY_SCORE';
export const UPDATE_TEMPLATE_PATIENT_SUBURB = 'pic/PatientEntry/UPDATE_TEMPLATE_PATIENT_SUBURB';
export const UPDATE_TEMPLATE_PATIENT_POSTCODE = 'pic/PatientEntry/UPDATE_TEMPLATE_PATIENT_POSTCODE';
export const UPDATE_TEMPLATE_PATIENT_GEOMETRY = 'pic/PatientEntry/UPDATE_TEMPLATE_PATIENT_GEOMETRY';
export const UPDATE_TEMPLATE_PATIENT_PREGNANT_STATUS = 'pic/PatientEntry/UPDATE_TEMPLATE_PATIENT_PREGNANT_STATUS';
export const UPDATE_TEMPLATE_PATIENT_PREGNANT_TRIMESTER = 'pic/PatientEntry/UPDATE_TEMPLATE_PATIENT_PREGNANT_TRIMESTER';
export const UPDATE_TEMPLATE_PATIENT_PREGNANT_TRIMESTER_UNKNOWN = 'pic/PatientEntry/UPDATE_TEMPLATE_PATIENT_PREGNANT_TRIMESTER_UNKNOWN';
export const UPDATE_TEMPLATE_PATIENT_PREGNANT_LACTATING = 'pic/PatientEntry/UPDATE_TEMPLATE_PATIENT_PREGNANT_LACTATING';
export const UPDATE_TEMPLATE_PATIENT_RISK_ASSESSMENT = 'pic/PatientEntry/UPDATE_TEMPLATE_PATIENT_RISK_ASSESSMENT';
export const UPDATE_TEMPLATE_PATIENT_SEX = 'pic/PatientEntry/UPDATE_TEMPLATE_PATIENT_SEX';
export const UPDATE_TEMPLATE_PATIENT_WEIGHT = 'pic/PatientEntry/UPDATE_TEMPLATE_PATIENT_WEIGHT';
