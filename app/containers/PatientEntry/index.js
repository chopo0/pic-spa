import { connect } from 'react-redux';
import _isEmpty from 'lodash/isEmpty';
import _isUndefined from 'lodash/isUndefined';

import { makeSelectCurrentCase, makeSelectSPI, selectVars, selectTemplates } from 'containers/App/selectors';

import PatientView from 'components/PatientView';

import {
  executeMacro,
  parseGoogleAddressComponent,
  updateAgeGroup as _updateAgeGroup,
  updateAgeYears as _updateAgeYears,
} from 'utils/helpers';

import {
  lookupMacroTemplate,
  updateDocument,
} from 'containers/App/actions';

import {
  updateCaseTopic,
} from 'containers/CaseEntry/actions/case';

import {
  updatePatientAge,
  updatePatientAgeGroup,
  updatePatientAgeYears,
  updatePatientAnimalType,
  updatePatientCategory,
  updatePatientCircumstance,
  updatePatientLocation,
  updatePatientMedicalRecordNumber,
  updatePatientName,
  updatePatientOccupation,
  updatePatientPoisonSeverityScore,
  updatePatientSuburb,
  updatePatientPostcode,
  updatePatientGeometry,
  updatePatientPregnantLactating,
  updatePatientPregnantStatus,
  updatePatientPregnantTrimester,
  updatePatientPregnantTrimesterUnknown,
  updatePatientRiskAssessment,
  updatePatientSex,
  updatePatientWeight,
} from './actions/case';

const makeMapStateToProps = () => {
  const selectCurrentCase = makeSelectCurrentCase();
  const selectSPI = makeSelectSPI();
  const mapStateToProps = (state) => {
    const vars = selectVars(state);
    const templates = selectTemplates(state);
    const { currentRecordId, callRecordId } = vars;
    const currentCase = selectCurrentCase(state);

    const currentComms = currentCase.communications[currentCase.communications.length - 1];

    const interlocutor = currentComms && currentComms.interlocutor ? currentComms.interlocutor : undefined;
    const interlocutorPostcode = (interlocutor && interlocutor.organisation) ? interlocutor.organisation.postcode : '';
    const interlocutorCity = (interlocutor && interlocutor.organisation) ? interlocutor.organisation.city : '';
    const interlocutorGeometry = (interlocutor && interlocutor.organisation) ? interlocutor.organisation.geometry : '';

    const topic = currentCase.topic ? currentCase.topic : '';
    const template = (!_isEmpty(vars.macro) && vars.macro in templates) ? templates[vars.macro] : undefined;

    const patient = currentCase.patient;
    const name = (patient && patient.name) ? patient.name : '';
    const category = (patient && patient.category) ? patient.category : '';
    const animalType = (patient && patient.animalType) ? patient.animalType : '';
    const sex = (patient && patient.gender) ? patient.gender : '';
    const age = (patient && patient.age) ? patient.age : '';
    const ageUnits = (patient && patient.ageUnits) ? patient.ageUnits : '';
    const ageGroup = (patient && patient.ageGroup) ? patient.ageGroup : '';
    const weight = (patient && patient.weight) ? patient.weight : 0;
    const weightUnits = (patient && patient.weightUnits) ? patient.weightUnits : '';
    const occupation = (patient && patient.occupation) ? patient.occupation : '';
    const circumstance = (patient && patient.circumstance) ? patient.circumstance : '';
    const location = (patient && patient.location) ? patient.location : '';
    const suburb = (patient && patient.suburb) ? patient.suburb : '';
    const postcode = (patient && patient.postcode) ? patient.postcode : '';
    const geometry = (patient && patient.geometry) ? patient.geometry : '';
    const medicalRecordNumber = (patient && patient.medicalRecordNumber) ? patient.medicalRecordNumber : '';
    const pregnantStatus = (patient && patient.pregnant && patient.pregnant.status) ? patient.pregnant.status : 'unknown';
    const pregnantTrimester = (patient && patient.pregnant && patient.pregnant.trimester) ? patient.pregnant.trimester : 'unknown';
    const pregnantTrimesterUnknown = (patient && patient.pregnant) ? patient.pregnant.trimesterUnknown : false;
    const pregnantLactating = (patient && patient.pregnant && patient.pregnant.lactating) ? patient.pregnant.lactating : 'unknown';
    const riskAssessment = (patient) ? patient.riskAssessment : '';
    const poisonSeverityScore = (patient) ? patient.poisonSeverityScore : '';

    return {
      currentRecordId,
      callRecordId,
      currentCase,

      topic,
      template,

      name,
      category,
      animalType,
      sex,
      age,
      ageUnits,
      ageGroup,
      weight,
      weightUnits,

      circumstance,
      location,
      suburb,
      postcode,
      geometry,

      occupation,
      medicalRecordNumber,

      pregnantStatus,
      pregnantTrimester,
      pregnantTrimesterUnknown,
      pregnantLactating,
      riskAssessment,
      poisonSeverityScore,

      interlocutorPostcode,
      interlocutorCity,
      interlocutorGeometry,

      orgCountry: vars.jwtClaims.org_country ? vars.jwtClaims.org_country : '',
      userSPI: selectSPI(state),
    };
  };
  return mapStateToProps;
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  updateCaseTopic: (old, topic) => {
    dispatch(updateCaseTopic(ownProps.recordId, old, topic));
  },

  updateName: (old, name) => {
    dispatch(updatePatientName(ownProps.recordId, old, name));
  },
  updateCategory: (old, category) => {
    dispatch(updatePatientCategory(ownProps.recordId, old, category));
  },
  updateAnimalType: (old, animalType) => {
    dispatch(updatePatientAnimalType(ownProps.recordId, old, animalType));
  },
  updateSex: (old, sex) => {
    dispatch(updatePatientSex(ownProps.recordId, old, sex));
  },
  updateAge: (oldAge, age, oldAgeUnits, ageUnits, oldAgeGroup, oldAgeYears) => {
    dispatch(updatePatientAge(ownProps.recordId, oldAge, age, oldAgeUnits, ageUnits));
    _updateAgeGroup(age, ageUnits, oldAgeGroup, ownProps, updatePatientAgeGroup, dispatch);
    _updateAgeYears(age, ageUnits, oldAgeYears, ownProps, updatePatientAgeYears, dispatch);
  },
  updateAgeGroup: (old, ageGroup) => {
    dispatch(updatePatientAgeGroup(ownProps.recordId, old, ageGroup));
  },
  updateWeight: (oldWeight, weight, oldWeightUnits, weightUnits) => {
    dispatch(updatePatientWeight(ownProps.recordId, oldWeight, weight, oldWeightUnits, weightUnits));
  },
  updateCircumstance: (old, circumstance) => {
    dispatch(updatePatientCircumstance(ownProps.recordId, old, circumstance));
  },
  updateLocation: (old, location) => {
    dispatch(updatePatientLocation(ownProps.recordId, old, location));
  },
  updateLocationFromLookup: (suggest, suburb, postcode, geometry) => {
    const map = new google.maps.Map(document.createElement('div')); // eslint-disable-line no-undef
    const placesService = new google.maps.places.PlacesService(map); // eslint-disable-line no-undef

    try {
      placesService.getDetails({ placeId: suggest.placeId }, (place) => {
        const address = parseGoogleAddressComponent(place.address_components);
        dispatch(updatePatientSuburb(ownProps.recordId, suburb, !_isUndefined(address.city) ? address.city : ''));
        dispatch(updatePatientPostcode(ownProps.recordId, postcode, !_isUndefined(address.zip) ? address.zip : ''));
        dispatch(updatePatientGeometry(ownProps.recordId, geometry, { lat: place.geometry.location.lat(), lng: place.geometry.location.lng() }));
      });
    } catch (e) {
      // No action
    }
  },
  updateSuburb: (old, suburb) => {
    dispatch(updatePatientSuburb(ownProps.recordId, old, suburb));
  },
  updatePostcode: (old, postcode) => {
    dispatch(updatePatientPostcode(ownProps.recordId, old, postcode));
  },
  updateGeometry: (old, geometry) => {
    dispatch(updatePatientGeometry(ownProps.recordId, old, geometry));
  },

  updateOccupation: (old, occupation) => {
    dispatch(updatePatientOccupation(ownProps.recordId, old, occupation));
  },
  updateMedicalRecordNumber: (old, medicalRecordNumber) => {
    dispatch(updatePatientMedicalRecordNumber(ownProps.recordId, old, medicalRecordNumber));
  },
  updatePregnantStatus: (old, pregnantStatus) => {
    dispatch(updatePatientPregnantStatus(ownProps.recordId, old, pregnantStatus));
  },
  updatePregnantTrimester: (old, pregnantTrimester) => {
    dispatch(updatePatientPregnantTrimester(ownProps.recordId, old, pregnantTrimester));
  },
  updatePregnantTrimesterUnknown: (old, pregnantTrimesterUnknown) => {
    dispatch(updatePatientPregnantTrimesterUnknown(ownProps.recordId, old, pregnantTrimesterUnknown));
  },
  updatePregnantLactating: (old, pregnantLactating) => {
    dispatch(updatePatientPregnantLactating(ownProps.recordId, old, pregnantLactating));
  },
  updateRiskAssessment: (old, riskAssessment) => {
    dispatch(updatePatientRiskAssessment(ownProps.recordId, old, riskAssessment));
  },
  updatePoisonSeverityScore: (old, poisonSeverityScore) => {
    dispatch(updatePatientPoisonSeverityScore(ownProps.recordId, old, poisonSeverityScore));
  },
  lookupMacroTemplate: (macro) => {
    dispatch(lookupMacroTemplate(macro));
  },
  executeMacro: (document, template) => {
    const updatedDocument = executeMacro(document, template);
    dispatch(updateDocument(updatedDocument));
  },
});

export default connect(
  makeMapStateToProps,
  mapDispatchToProps
)(PatientView);
