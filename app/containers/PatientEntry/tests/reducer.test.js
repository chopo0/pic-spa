/* eslint-disable */

import PatientReducer from '../reducer';

import {
  updatePatientAge,
  updatePatientAgeGroup,
  updatePatientAgeYears,
  updatePatientAnimalType,
  updatePatientCategory,
  updatePatientCircumstance,
  updatePatientLocation,
  updatePatientMedicalRecordNumber,
  updatePatientName,
  updatePatientOccupation,
  updatePatientPoisonSeverityScore,
  updatePatientSuburb,
  updatePatientPostcode,
  updatePatientRiskAssessment,
  updatePatientSex,
  updatePatientWeight,
} from '../actions/case';

describe('PatientReducer', () => {
  it('returns the initial state', () => {
    expect(PatientReducer(undefined, {})).toMatchSnapshot();
  });
  it('update patient age', () => {
    expect(PatientReducer({}, updatePatientAge(1, 10, 1, 'months', 'years'))).toMatchSnapshot();
  });
  it('update patient age group', () => {
    expect(PatientReducer({}, updatePatientAgeGroup(1, 'months', 'years'))).toMatchSnapshot();
  });
  it('update patient age years', () => {
    expect(PatientReducer({}, updatePatientAgeYears(1, '', '1.5'))).toMatchSnapshot();
  });
  it('update patient animal type', () => {
    expect(PatientReducer({}, updatePatientAnimalType(1, 'dog', 'bird'))).toMatchSnapshot();
  });
  it('clear patient animal type if human category', () => {
    expect(PatientReducer({'category': 'bird'}, updatePatientCategory(1, 'animal', 'human'))).toMatchSnapshot();
  });
  it('update patient category', () => {
    expect(PatientReducer({}, updatePatientCategory(1, 'human', 'animal'))).toMatchSnapshot();
  });
  it('update patient circumstance', () => {
    expect(PatientReducer({}, updatePatientCircumstance(1, 'Old circumstance', 'New circumstance'))).toMatchSnapshot();
  });
  it('update patient location', () => {
    expect(PatientReducer({}, updatePatientLocation(1, 'Work', 'Hospital'))).toMatchSnapshot();
  });
  it('update patient MRN', () => {
    expect(PatientReducer({}, updatePatientMedicalRecordNumber(1, 5678, 4321))).toMatchSnapshot();
  });
  it('update patient name', () => {
    expect(PatientReducer({}, updatePatientName(1, 'Joe Blogs', 'John Smith'))).toMatchSnapshot();
  });
  it('update patient occupation', () => {
    expect(PatientReducer({}, updatePatientOccupation(1, 'Student', 'Salesperson'))).toMatchSnapshot();
  });
  it('update patient poison severity score', () => {
    expect(PatientReducer({}, updatePatientPoisonSeverityScore(1, 3, 4))).toMatchSnapshot();
  });
  it('update patient suburb', () => {
    expect(PatientReducer({}, updatePatientSuburb(1, 'Perth', 'Sydney'))).toMatchSnapshot();
  });
  it('update patient postcode', () => {
    expect(PatientReducer({}, updatePatientPostcode(1, 3000, 6000))).toMatchSnapshot();
  });
  it('update patient risk assessment', () => {
    expect(PatientReducer({}, updatePatientRiskAssessment(1, 'Old assessment', 'New assessment'))).toMatchSnapshot();
  });
  it('update patient sex', () => {
    expect(PatientReducer({}, updatePatientSex(1, 'female', 'male'))).toMatchSnapshot();
  });
  it('update patient weight', () => {
    expect(PatientReducer({}, updatePatientWeight(1, 65, 68, 'kg', 'kg'))).toMatchSnapshot();
  });
});
