import {
  updatePatientAge,
  updatePatientAgeGroup,
  updatePatientAgeYears,
  updatePatientAnimalType,
  updatePatientCategory,
  updatePatientCircumstance,
  updatePatientLocation,
  updatePatientMedicalRecordNumber,
  updatePatientName,
  updatePatientOccupation,
  updatePatientPoisonSeverityScore,
  updatePatientSuburb,
  updatePatientPostcode,
  updatePatientPregnantLactating,
  updatePatientPregnantStatus,
  updatePatientPregnantTrimester,
  updatePatientPregnantTrimesterUnknown,
  updatePatientRiskAssessment,
  updatePatientSex,
  updatePatientWeight,
} from '../../actions/case';

describe('Patient action creators', () => {
  it('updatePatientAge', () => {
    expect(updatePatientAge(1, 10, 20, 'months', 'years')).toMatchSnapshot();
  });
  it('updatePatientAgeGroup', () => {
    expect(updatePatientAgeGroup(1, 'months', 'years')).toMatchSnapshot();
  });
  it('updatePatientAgeYears', () => {
    expect(updatePatientAgeYears(1, '', '0.75')).toMatchSnapshot();
  });
  it('updatePatientAnimalType', () => {
    expect(updatePatientAnimalType(1, 'dog', 'bird')).toMatchSnapshot();
  });
  it('updatePatientCategory', () => {
    expect(updatePatientCategory(1, 'animal', 'human')).toMatchSnapshot();
  });
  it('updatePatientCircumstance', () => {
    expect(updatePatientCircumstance(1, 'Old Circumstance', 'New Circumstance')).toMatchSnapshot();
  });
  it('updatePatientLocation', () => {
    expect(updatePatientLocation(1, 'Old Location', 'New Location')).toMatchSnapshot();
  });
  it('updatePatientMedicalRecordNumber', () => {
    expect(updatePatientMedicalRecordNumber(1, '1234', '5678')).toMatchSnapshot();
  });
  it('updatePatientName', () => {
    expect(updatePatientName(1, 'Old Name', 'New Name')).toMatchSnapshot();
  });
  it('updatePatientOccupation', () => {
    expect(updatePatientOccupation(1, 'Old Occupation', 'New Occupation')).toMatchSnapshot();
  });
  it('updatePatientPoisonSeverityScore', () => {
    expect(updatePatientPoisonSeverityScore(1, 'Minimal', 'Minor')).toMatchSnapshot();
  });
  it('updatePatientSuburb', () => {
    expect(updatePatientSuburb(1, 'Perth', 'Sydney')).toMatchSnapshot();
  });
  it('updatePatientPostcode', () => {
    expect(updatePatientPostcode(1, '6000', '3000')).toMatchSnapshot();
  });
  it('updatePatientPregnantLactating', () => {
    expect(updatePatientPregnantLactating(1, 'No', 'Yes')).toMatchSnapshot();
  });
  it('updatePatientPregnantStatus', () => {
    expect(updatePatientPregnantStatus(1, 'No', 'Yes')).toMatchSnapshot();
  });
  it('updatePatientPregnantTrimester', () => {
    expect(updatePatientPregnantTrimester(1, '1', '2')).toMatchSnapshot();
  });
  it('updatePatientPregnantTrimesterUnknown', () => {
    expect(updatePatientPregnantTrimesterUnknown(1, false, true)).toMatchSnapshot();
  });
  it('updatePatientRiskAssessment', () => {
    expect(updatePatientRiskAssessment(1, 'Old Assessment', 'New Assessment')).toMatchSnapshot();
  });
  it('updatePatientSex', () => {
    expect(updatePatientSex(1, 'male', 'female')).toMatchSnapshot();
  });
  it('updatePatientWeight', () => {
    expect(updatePatientWeight(1, '80', '83', 'kg', 'kg')).toMatchSnapshot();
  });
});
