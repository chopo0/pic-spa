import { connect } from 'react-redux';
import _isEmpty from 'lodash/isEmpty';
import _isUndefined from 'lodash/isUndefined';

import { selectVars, selectTemplates, makeSelectCurrentTemplate, makeSelectEditingTemplate, makeSelectSPI } from 'containers/App/selectors';

import PatientView from 'components/PatientView';

import {
  updateAgeGroup as _updateAgeGroup,
  updateAgeYears as _updateAgeYears,
  parseGoogleAddressComponent,
  isValidMacro,
} from 'utils/helpers';

import {
  lookupMacroTemplate,
} from 'containers/App/actions';

import {
  updateTemplateMacro,
  updateTemplateTopic,
} from 'containers/CaseEntry/actions/template';

import {
  updateTemplatePatientAge,
  updateTemplatePatientAgeGroup,
  updateTemplatePatientAgeYears,
  updateTemplatePatientAnimalType,
  updateTemplatePatientCategory,
  updateTemplatePatientCircumstance,
  updateTemplatePatientLocation,
  updateTemplatePatientMedicalRecordNumber,
  updateTemplatePatientName,
  updateTemplatePatientOccupation,
  updateTemplatePatientPoisonSeverityScore,
  updateTemplatePatientSuburb,
  updateTemplatePatientPostcode,
  updateTemplatePatientGeometry,
  updateTemplatePatientPregnantLactating,
  updateTemplatePatientPregnantStatus,
  updateTemplatePatientPregnantTrimester,
  updateTemplatePatientPregnantTrimesterUnknown,
  updateTemplatePatientRiskAssessment,
  updateTemplatePatientSex,
  updateTemplatePatientWeight,
} from './actions/template';

const makeMapStateToProps = () => {
  const selectSPI = makeSelectSPI();
  const mapStateToProps = (state) => {
    const vars = selectVars(state);
    const templates = selectTemplates(state);
    const { editingTemplateId, currentTemplateId } = vars;
    const currentTemplate = !_isEmpty(editingTemplateId) ? makeSelectEditingTemplate()(state) : makeSelectCurrentTemplate()(state);

    const topic = currentTemplate.topic ? currentTemplate.topic : '';
    const macro = vars.editingTemplateMacro.current ? vars.editingTemplateMacro.current : '';

    const patient = currentTemplate.patient;
    const name = (patient && patient.name) ? patient.name : '';
    const category = (patient && patient.category) ? patient.category : '';
    const animalType = (patient && patient.animalType) ? patient.animalType : '';
    const sex = (patient && patient.gender) ? patient.gender : '';
    const age = (patient && patient.age) ? patient.age : '';
    const ageUnits = (patient && patient.ageUnits) ? patient.ageUnits : '';
    const ageGroup = (patient && patient.ageGroup) ? patient.ageGroup : '';
    const weight = (patient && patient.weight) ? patient.weight : 0;
    const weightUnits = (patient && patient.weightUnits) ? patient.weightUnits : '';
    const occupation = (patient && patient.occupation) ? patient.occupation : '';
    const circumstance = (patient && patient.circumstance) ? patient.circumstance : '';
    const location = (patient && patient.location) ? patient.location : '';
    const suburb = (patient && patient.suburb) ? patient.suburb : '';
    const postcode = (patient && patient.postcode) ? patient.postcode : '';
    const geometry = (patient && patient.geometry) ? patient.geometry : '';
    const medicalRecordNumber = (patient && patient.medicalRecordNumber) ? patient.medicalRecordNumber : '';
    const pregnantStatus = (patient && patient.pregnant && patient.pregnant.status) ? patient.pregnant.status : 'unknown';
    const pregnantTrimester = (patient && patient.pregnant && patient.pregnant.trimester) ? patient.pregnant.trimester : 'unknown';
    const pregnantTrimesterUnknown = (patient && patient.pregnant) ? patient.pregnant.trimesterUnknown : false;
    const pregnantLactating = (patient && patient.pregnant && patient.pregnant.lactating) ? patient.pregnant.lactating : 'unknown';
    const riskAssessment = (patient) ? patient.riskAssessment : '';
    const poisonSeverityScore = (patient) ? patient.poisonSeverityScore : '';

    return {
      currentTemplateId,
      editingTemplateId,

      topic,
      macro,
      isValidMacro: isValidMacro(vars.uiMode, vars.editingTemplateMacro.initial, vars.editingTemplateMacro.current, templates),

      name,
      category,
      animalType,
      sex,
      age,
      ageUnits,
      ageGroup,
      weight,
      weightUnits,

      circumstance,
      location,
      suburb,
      postcode,
      geometry,

      occupation,
      medicalRecordNumber,

      pregnantStatus,
      pregnantTrimester,
      pregnantTrimesterUnknown,
      pregnantLactating,
      riskAssessment,
      poisonSeverityScore,
      orgCountry: vars.jwtClaims.org_country ? vars.jwtClaims.org_country : '',

      userSPI: selectSPI(state),
    };
  };
  return mapStateToProps;
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  updateCaseTopic: (old, topic) => {
    dispatch(updateTemplateTopic(ownProps.recordId, old, topic));
  },

  updateName: (old, name) => {
    dispatch(updateTemplatePatientName(ownProps.recordId, old, name));
  },
  updateCategory: (old, category) => {
    dispatch(updateTemplatePatientCategory(ownProps.recordId, old, category));
  },
  updateAnimalType: (old, animalType) => {
    dispatch(updateTemplatePatientAnimalType(ownProps.recordId, old, animalType));
  },
  updateSex: (old, sex) => {
    dispatch(updateTemplatePatientSex(ownProps.recordId, old, sex));
  },
  updateAge: (oldAge, age, oldAgeUnits, ageUnits, oldAgeGroup, oldAgeYears) => {
    dispatch(updateTemplatePatientAge(ownProps.recordId, oldAge, age, oldAgeUnits, ageUnits));
    _updateAgeGroup(age, ageUnits, oldAgeGroup, ownProps, updateTemplatePatientAgeGroup, dispatch);
    _updateAgeYears(age, ageUnits, oldAgeYears, ownProps, updateTemplatePatientAgeYears, dispatch);
  },
  updateAgeGroup: (old, ageGroup) => {
    dispatch(updateTemplatePatientAgeGroup(ownProps.recordId, old, ageGroup));
  },
  updateWeight: (oldWeight, weight, oldWeightUnits, weightUnits) => {
    dispatch(updateTemplatePatientWeight(ownProps.recordId, oldWeight, weight, oldWeightUnits, weightUnits));
  },
  updateCircumstance: (old, circumstance) => {
    dispatch(updateTemplatePatientCircumstance(ownProps.recordId, old, circumstance));
  },
  updateLocation: (old, location) => {
    dispatch(updateTemplatePatientLocation(ownProps.recordId, old, location));
  },
  updateLocationFromLookup: (suggest, suburb, postcode, geometry) => {
    const map = new google.maps.Map(document.createElement('div')); // eslint-disable-line no-undef
    const placesService = new google.maps.places.PlacesService(map); // eslint-disable-line no-undef

    try {
      placesService.getDetails({ placeId: suggest.placeId }, (place) => {
        const address = parseGoogleAddressComponent(place.address_components);
        if (!_isUndefined(address.city)) {
          dispatch(updateTemplatePatientSuburb(ownProps.recordId, suburb, address.city));
        }
        if (!_isUndefined(address.zip)) {
          dispatch(updateTemplatePatientPostcode(ownProps.recordId, postcode, address.zip));
        }
        dispatch(updateTemplatePatientGeometry(ownProps.recordId, geometry, { lat: place.geometry.location.lat(), lng: place.geometry.location.lng() }));
      });
    } catch (e) {
      // No action
    }
  },
  updateSuburb: (old, suburb) => {
    dispatch(updateTemplatePatientSuburb(ownProps.recordId, old, suburb));
  },
  updatePostcode: (old, postcode) => {
    dispatch(updateTemplatePatientPostcode(ownProps.recordId, old, postcode));
  },
  updateOccupation: (old, occupation) => {
    dispatch(updateTemplatePatientOccupation(ownProps.recordId, old, occupation));
  },
  updateMedicalRecordNumber: (old, medicalRecordNumber) => {
    dispatch(updateTemplatePatientMedicalRecordNumber(ownProps.recordId, old, medicalRecordNumber));
  },
  updatePregnantStatus: (old, pregnantStatus) => {
    dispatch(updateTemplatePatientPregnantStatus(ownProps.recordId, old, pregnantStatus));
  },
  updatePregnantTrimester: (old, pregnantTrimester) => {
    dispatch(updateTemplatePatientPregnantTrimester(ownProps.recordId, old, pregnantTrimester));
  },
  updatePregnantTrimesterUnknown: (old, pregnantTrimesterUnknown) => {
    dispatch(updateTemplatePatientPregnantTrimesterUnknown(ownProps.recordId, old, pregnantTrimesterUnknown));
  },
  updatePregnantLactating: (old, pregnantLactating) => {
    dispatch(updateTemplatePatientPregnantLactating(ownProps.recordId, old, pregnantLactating));
  },
  updateRiskAssessment: (old, riskAssessment) => {
    dispatch(updateTemplatePatientRiskAssessment(ownProps.recordId, old, riskAssessment));
  },
  updatePoisonSeverityScore: (old, poisonSeverityScore) => {
    dispatch(updateTemplatePatientPoisonSeverityScore(ownProps.recordId, old, poisonSeverityScore));
  },
  lookupMacroTemplate: (macro) => {
    dispatch(lookupMacroTemplate(macro));
  },
  updateTemplateMacro: (old, macro) => {
    dispatch(updateTemplateMacro(ownProps.recordId, old, macro));
  },
});

export default connect(
  makeMapStateToProps,
  mapDispatchToProps
)(PatientView);
