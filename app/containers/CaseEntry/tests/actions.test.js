/* eslint-disable */

import casesReducer from 'containers/CaseEntry/reducers/cases';

import {
  addCallRecord,
  addCase,
  addNoteAttachments,
  endCall,
  linkNote,
  removeNoteAttachment,
  resetAgentViewCentreName,
  saveNote,
  updateAgentViewCentreName,
  updateCaseStatus,
  updateCaseTopic,
  updateCaseType,
  updateNoteContent,
  updateNoteContext,
  updateNoteSubcontext,
  updateNoteTitle,
} from '../actions/case';

import {
  updateTemplateTopic,
} from '../actions/template';

describe('App Actions', () => {
  const mockedDate = new Date(2018, 2, 4, 4, 59, 13, 805);
  global.Date = jest.fn(() => mockedDate);
  describe('App action creators', () => {
    it('addCallRecord', () => {
      const action = addCallRecord(1, { name: 'Test SPI' });
      action.commsNoteId = 'de6d38464f8ee145f0731aca7faa24f78d07e25f';
      expect(action).toMatchSnapshot();
    });
    it('addCase', () => {
      const action = addCase({ name: 'Test SPI' }, 'WAPIC');
      action.id = '481966f3ebcc4cef9a26ad178dd3e4cfce6e9aef';
      action.commsNoteId = 'de6d38464f8ee145f0731aca7faa24f78d07e25f';
      expect(action).toMatchSnapshot();
    });
    it('addNoteAttachments', () => {
      expect(casesReducer({'bbb' : {'_id': 'bbb', '_attachments': {}, 'notes': {aaa: {}}}}, addNoteAttachments('bbb', 'aaa', [{ name: 'filename', type: 'pdf' }]))).toMatchSnapshot();
    });
    it('endCall', () => {
      expect(endCall()).toMatchSnapshot();
    });
    it('linkNote', () => {
      expect(linkNote(1, 2)).toMatchSnapshot();
    });
    it('removeNoteAttachment', () => {
      expect(casesReducer({'bbb': {'_id': 'bbb', 'notes': {aaa: {attachments: ['filename']}}}}, removeNoteAttachment('bbb', 'aaa', 0, 'filename'))).toMatchSnapshot();
    });
    it('resetAgentViewCentreName', () => {
      expect(resetAgentViewCentreName()).toMatchSnapshot();
    });
    it('saveNote', () => {
      expect(saveNote(1, 2, 'Test context', 'Test subcontext', 'Test title', 'Test content', [{ name: 'Test Attachment' }], { name: 'Test SPI' })).toMatchSnapshot();
    });
    it('updateAgentViewCentreName', () => {
      expect(updateAgentViewCentreName('Test name')).toMatchSnapshot();
    });
    it('updateCaseStatus', () => {
      expect(updateCaseStatus(1, 'open', { name: 'Test SPI' }, false)).toMatchSnapshot();
    });
    it('updateCaseTopic', () => {
      expect(updateCaseTopic(1, 'Old Topic', 'New Topic')).toMatchSnapshot();
    });
    it('updateCaseType', () => {
      expect(updateCaseType(1, 'hoax')).toMatchSnapshot();
    });
    it('updateNoteContent', () => {
      expect(casesReducer({'bbb': {'_id': 'bbb', 'notes': {aaa: { content: 'Old Content' }}}}, updateNoteContent('bbb', 'aaa', 'New Content'))).toMatchSnapshot();
    });
    it('updateNoteContext', () => {
      expect(casesReducer({'bbb': {'_id': 'bbb', 'notes': {aaa: { context: 'Old Context' }}}}, updateNoteContext('bbb', 'aaa', 'New Context'))).toMatchSnapshot();
    });
    it('updateNoteContext', () => {
      expect(casesReducer({'bbb': {'_id': 'bbb', 'notes': {aaa: { subcontext: 'Old Subcontext' }}}}, updateNoteSubcontext('bbb', 'aaa', 'New Subcontext'))).toMatchSnapshot();
    });
    it('updateNoteTitle', () => {
      expect(casesReducer({'bbb': {'_id': 'bbb', 'notes': {aaa: { title: 'Old Title' }}}}, updateNoteTitle('bbb', 'aaa', 'New Title'))).toMatchSnapshot();
    });
    it('updateTemplateTopic', () => {
      expect(updateTemplateTopic(1, 'Old Topic', 'New Topic')).toMatchSnapshot();
    });
  });
});
