/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  'action.menu': {
    id: 'pic.components.DashboardPage.action.menu',
    defaultMessage: 'Menu',
  },
  'action.app': {
    id: 'pic.components.DashboardPage.action.app',
    defaultMessage: 'App',
  },
  'action.analytics': {
    id: 'pic.components.DashboardPage.action.analytics',
    defaultMessage: 'Analytics',
  },
  'action.admin': {
    id: 'pic.components.DashboardPage.action.admin',
    defaultMessage: 'Admin',
  },
  'action.signout': {
    id: 'pic.components.DashboardPage.action.signout',
    defaultMessage: 'Logout',
  },
});
