import React from 'react';
import { connect } from 'react-redux';

import PouchDB from 'pouchdb';
import PouchdbFind from 'pouchdb-find';

import { Radar } from 'react-chartjs-2';

import moment from 'moment/moment';
import times from 'lodash/times';
import zipObject from 'lodash/zipObject';

PouchDB.plugin(PouchdbFind);

class CallsByDay extends React.Component {
  constructor() {
    super();

    this.state = {
      data: [],
    };

    const db = new PouchDB('PicApp');

    db.query({
      map(doc, emit) {
        for (let i = 0; i < doc.communications.length; i += 1) {
          if (!doc.communications[i].deleted && moment(doc.communications[i].timestamp) > moment().subtract(1, 'week')) {
            emit((moment(doc.communications[i].timestamp).startOf('day')).toISOString(), 1);
          }
        }
      },
      reduce: '_sum',
    }, {
      reduce: true,
      group: true,
      group_level: 1,
    }, (err, response) => {
      const raw = zipObject(
          times(7, (i) => (moment().subtract(7 - (i + 1), 'day').startOf('day').toISOString())),
          times(7, () => 0)
      );
      response.rows.map((r) => {
        raw[moment(r.key).startOf('day').toISOString()] = r.value;
        return r;
      });
      const labels = [];
      const data = [];
      Object.keys(raw).sort().map((key) => {
        labels.push(moment(key).format('ddd'));
        data.push(raw[key]);
        return key;
      });
      this.setState({ labels, data });
    });
  }

  componentDidMount() {
  }

  render() {
    return (
      <div style={{ position: 'relative', height: '35vh' }}>
        <Radar
          data={{
            labels: this.state.labels,
            datasets: [{
              label: 'Calls',
              data: this.state.data,
              backgroundColor: 'rgba(61, 163, 232, 0.5)',
              borderColor: 'rgba(61, 163, 232, 1)',
              borderWidth: 1,
              pointColor: 'rgba(61, 163, 232, 1)',
              pointBackgroundColor: 'rgba(61, 163, 232, 1)',
            }],
          }}
          options={{
            responsive: true,
            maintainAspectRatio: false,
            animationEasing: 'easeInSine',
            showTooltips: true,
          }}
        />
      </div>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(CallsByDay);
