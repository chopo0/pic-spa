import React from 'react';
import { connect } from 'react-redux';

import PouchDB from 'pouchdb';
import PouchdbFind from 'pouchdb-find';

import { HorizontalBar } from 'react-chartjs-2';

import moment from 'moment/moment';
import times from 'lodash/times';
import zipObject from 'lodash/zipObject';

PouchDB.plugin(PouchdbFind);

function getStateFromPostcode(postcode) {
  switch (true) {
    case (postcode >= 1000 && postcode <= 1999):
    case (postcode >= 2000 && postcode <= 2599):
    case (postcode >= 2620 && postcode <= 2899):
    case (postcode >= 2921 && postcode <= 2999):
      return 'NSW';
    case (postcode >= 200 && postcode <= 299):
    case (postcode >= 2600 && postcode <= 2619):
    case (postcode >= 2900 && postcode <= 2920):
      return 'ACT';
    case (postcode >= 3000 && postcode <= 3999):
    case (postcode >= 8000 && postcode <= 8999):
      return 'VIC';
    case (postcode >= 4000 && postcode <= 4999):
    case (postcode >= 9000 && postcode <= 9999):
      return 'QLD';
    case (postcode >= 5000 && postcode <= 5799):
    case (postcode >= 5800 && postcode <= 5999):
      return 'SA';
    case (postcode >= 6000 && postcode <= 6797):
    case (postcode >= 6800 && postcode <= 6999):
      return 'WA';
    case (postcode >= 7000 && postcode <= 7799):
    case (postcode >= 7800 && postcode <= 7999):
      return 'TAS';
    case (postcode >= 800 && postcode <= 899):
    case (postcode >= 900 && postcode <= 999):
      return 'NT';
    default:
      return 'unknown';
  }
}

class CasesByStateThisMonth extends React.Component {
  constructor() {
    super();

    this.state = {
      labels: [],
      dataset: [],
    };

    const db = new PouchDB('PicApp');

    db.query({
      map(doc, emit) {
        if (!doc.communications[0].deleted && moment(doc.communications[0].timestamp) > moment().subtract(1, 'month')) {
          emit(getStateFromPostcode(parseInt(doc.postcode, 10)), 1);
        }
      },
      reduce: '_sum',
    }, {
      reduce: true,
      group: true,
      group_level: 1,
    }, (err, response) => {
      const states = ['NSW', 'ACT', 'VIC', 'QLD', 'SA', 'WA', 'TAS', 'NT', 'unknown'];
      const raw = zipObject(
          states,
          times(states.length, () => 0)
      );
      response.rows.map((r) => {
        raw[r.key] = r.value;
        return r;
      });
      const labels = [];
      const dataset = [];
      Object.keys(raw).sort().map((key) => {
        labels.push(key);
        dataset.push(raw[key]);
        return key;
      });
      this.setState({ labels, dataset });
    });
  }

  render() {
    const data = {
      labels: this.state.labels,
      datasets: [
        {
          label: 'Cases',
          backgroundColor: 'rgba(61,163,232,0.2)',
          borderColor: 'rgba(61,163,232,1)',
          borderWidth: 1,
          hoverBackgroundColor: 'rgba(61,163,232,0.4)',
          hoverBorderColor: 'rgba(61,163,232,1)',
          data: this.state.dataset,
        },
      ],
    };

    return (
      <div style={{ position: 'relative', height: '35vh' }}>
        <HorizontalBar
          data={data}
          options={{
            responsive: true,
            maintainAspectRatio: false,
            animationSteps: 300,
          }}
        />
      </div>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(CasesByStateThisMonth);
