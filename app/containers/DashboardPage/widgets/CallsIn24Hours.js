import React from 'react';
import { connect } from 'react-redux';

import PouchDB from 'pouchdb';
import PouchdbFind from 'pouchdb-find';

import { Bar } from 'react-chartjs-2';

import moment from 'moment/moment';
import times from 'lodash/times';
import zipObject from 'lodash/zipObject';

PouchDB.plugin(PouchdbFind);

class CallsIn24Hours extends React.Component {
  constructor() {
    super();

    this.state = {
      labels: [],
      dataset: [],
    };

    const db = new PouchDB('PicApp');

    db.query({
      map(doc, emit) {
        for (let i = 0; i < doc.communications.length; i += 1) {
          if (!doc.communications[i].deleted && moment(doc.communications[i].timestamp) > moment().subtract(1, 'day')) {
            emit((moment(doc.communications[i].timestamp).startOf('hour')).toISOString(), 1);
          }
        }
      },
      reduce: '_sum',
    }, {
      reduce: true,
      group: true,
      group_level: 1,
    }, (err, response) => {
      const raw = zipObject(
          times(24, (i) => (moment().subtract(24 - (i + 1), 'hour').startOf('hour').toISOString())),
          times(24, () => 0)
      );
      response.rows.map((r) => {
        raw[moment(r.key).startOf('hour').toISOString()] = r.value;
        return r;
      });
      const labels = [];
      const dataset = [];
      Object.keys(raw).sort().map((key) => {
        labels.push(moment(key).format('HH'));
        dataset.push(raw[key]);
        return key;
      });
      this.setState({ labels, dataset });
    });
  }

  render() {
    const data = {
      labels: this.state.labels,
      datasets: [
        {
          label: 'Calls',
          backgroundColor: 'rgba(255,99,132,0.2)',
          borderColor: 'rgba(255,99,132,1)',
          borderWidth: 1,
          hoverBackgroundColor: 'rgba(255,99,132,0.4)',
          hoverBorderColor: 'rgba(255,99,132,1)',
          data: this.state.dataset,
        },
      ],
    };

    return (
      <div style={{ position: 'relative', height: '35vh' }}>
        <Bar
          data={data}
          options={{
            responsive: true,
            maintainAspectRatio: false,
            animationSteps: 300,
          }}
        />
      </div>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(CallsIn24Hours);
