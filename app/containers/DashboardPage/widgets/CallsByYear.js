import React from 'react';
import { connect } from 'react-redux';

import PouchDB from 'pouchdb';
import PouchdbFind from 'pouchdb-find';

import { Line } from 'react-chartjs-2';

import moment from 'moment/moment';
import times from 'lodash/times';
import zipObject from 'lodash/zipObject';

PouchDB.plugin(PouchdbFind);

class CallsByDay extends React.Component {
  constructor() {
    super();

    this.state = {
      labels: [],
      data: [],
    };

    const db = new PouchDB('PicApp');

    db.query({
      map(doc, emit) {
        for (let i = 0; i < doc.communications.length; i += 1) {
          if (!doc.communications[i].deleted && moment(doc.communications[i].timestamp) > moment().subtract(5, 'year')) {
            emit((moment(doc.communications[i].timestamp).startOf('year')).toISOString(), 1);
          }
        }
        return doc;
      },
      reduce: '_sum',
    }, {
      reduce: true,
      group: true,
      group_level: 1,
    }, (err, response) => {
      const raw = zipObject(
          times(5, (i) => (moment().subtract(5 - (i + 1), 'year').startOf('year').toISOString())),
          times(5, () => 0)
      );
      response.rows.map((r) => {
        raw[moment(r.key).startOf('year').toISOString()] = r.value;
        return r;
      });
      const labels = [];
      const data = [];
      Object.keys(raw).sort().map((key) => {
        labels.push(moment(key).format('YYYY'));
        data.push(raw[key]);
        return key;
      });
      this.setState({ labels, data });
    });
  }

  componentDidMount() {
  }

  render() {
    return (
      <div style={{ position: 'relative', height: '35vh' }}>
        <Line
          data={{
            labels: this.state.labels,
            datasets: [{
              label: 'Calls',
              data: this.state.data,
              backgroundColor: 'rgba(81, 192, 191, 0.2)',
              borderColor: 'rgba(81, 192, 191, 1)',
              borderWidth: 1,
              pointColor: 'rgba(81, 192, 191, 1)',
              pointBackgroundColor: 'rgba(81, 192, 191, 1)',
            }],
          }}
          options={{
            responsive: true,
            maintainAspectRatio: false,
            animationSteps: 300,
          }}
        />
      </div>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(CallsByDay);
