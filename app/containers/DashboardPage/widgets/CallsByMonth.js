import React from 'react';
import styled from 'styled-components';
import colors from 'nice-color-palettes'; // eslint-disable-line import/extensions
import { Pie } from 'react-chartjs-2';

import moment from 'moment/moment';
import times from 'lodash/times';
import zipObject from 'lodash/zipObject';

import PouchDB from 'pouchdb';
import PouchdbFind from 'pouchdb-find';
import { connect } from 'react-redux';

PouchDB.plugin(PouchdbFind);

const StyledChart = styled.div`
  & .pie-legend {
    margin-top: 1em;
    list-style; none;
    font-size: 0.85rem;
  }
  
  & .pie-legend li {
    display: inline-block;
    margin-right: 0.5em;
  }
  
  & .pie-legend li .pie-legend-icon {
    width: 0.75em;
    height: 0.75em;
    display: inline-block;
    margin-right: 0.25em;
  }
`;

class CallsByMonth extends React.Component {
  constructor() {
    super();

    this.state = {
      labels: [],
      data: [],
    };

    const db = new PouchDB('PicApp');

    db.query({
      map(doc, emit) {
        for (let i = 0; i < doc.communications.length; i += 1) {
          if (!doc.communications[i].deleted && moment(doc.communications[i].timestamp) > moment().subtract(1, 'year')) {
            emit((moment(doc.communications[i].timestamp).startOf('month')).toISOString(), 1);
          }
        }
      },
      reduce: '_sum',
    }, {
      reduce: true,
      group: true,
      group_level: 1,
    }, (err, response) => {
      const raw = zipObject(
          times(12, (i) => (moment().subtract(12 - (i + 1), 'month').startOf('month').toISOString())),
          times(12, () => 0)
      );
      response.rows.map((r) => {
        raw[moment(r.key).startOf('month').toISOString()] = r.value;
        return r;
      });
      const labels = [];
      const data = [];
      Object.keys(raw).sort().map((key) => {
        labels.push(moment(key).format('MMM').toUpperCase());
        data.push(raw[key]);
        return key;
      });
      this.setState({ labels, data });

      /* let legend = this.refs.chart.getChart().generateLegend();
      this.setState({legend});*/
    });
  }

  componentDidMount() {
  }

  render() {
    return (
      <StyledChart style={{ position: 'relative', height: '35vh' }}>
        <div style={{ position: 'relative', height: '32vh' }}>
          <Pie
            data={{
              labels: this.state.labels,
              datasets: [{
                data: this.state.data,
                backgroundColor: colors[0].concat(colors[1], colors[2]),
              }],
            }}
            legend={{
              labels: {
                boxWidth: 8,
                fontSize: 8,
                padding: 5,
              },
            }}
            options={{
              responsive: true,
              maintainAspectRatio: false,
              animationEasing: 'easeInSine',
              showTooltips: true,
            }}
          />
        </div>
      </StyledChart>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(CallsByMonth);
