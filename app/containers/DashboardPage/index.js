import React from 'react';
import PropTypes from 'prop-types';
import { LinkContainer } from 'react-router-bootstrap';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import 'react-dazzle/lib/style/style.css';

import Grid from 'react-bootstrap/lib/Grid';
import Navbar from 'react-bootstrap/lib/Navbar';
import DropdownButton from 'react-bootstrap/lib/DropdownButton';
import MenuItem from 'react-bootstrap/lib/MenuItem';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';

import Dashboard from 'react-dazzle';

import { logout as logoutAction } from 'containers/App/actions';

import { makeSelectSPI } from 'containers/App/selectors';

import { userCan } from 'utils/helpers';

import messages from './messages';

import CallsIn24Hours from './widgets/CallsIn24Hours';
import CallsByMonth from './widgets/CallsByMonth';
import CallsByDay from './widgets/CallsByDay';
import CallsByYear from './widgets/CallsByYear';
import CasesByStateThisMonth from './widgets/CasesByStateThisMonth';

class DashboardPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      widgets: {
        CallsInLastDay: {
          type: CallsIn24Hours,
          title: 'Calls over the last day',
        },
        CallsByMonth: {
          type: CallsByMonth,
          title: 'Calls by month',
        },
        CallsByDay: {
          type: CallsByDay,
          title: 'Calls by day',
        },
        CallsByYear: {
          type: CallsByYear,
          title: 'Calls by year',
        },
        CasesByStateThisMonth: {
          type: CasesByStateThisMonth,
          title: 'Cases by State this month',
        },
      },
      layout: {
        rows: [{
          columns: [{
            className: 'col-md-8 col-sm-8 col-xs-8',
            widgets: [{ key: 'CallsInLastDay' }],
          }, {
            className: 'col-md-4 col-sm-4 col-xs-4',
            widgets: [{ key: 'CallsByMonth' }],
          }],
        }, {
          columns: [{
            className: 'col-md-4 col-sm-4 col-xs-4',
            widgets: [{ key: 'CallsByDay' }],
          }, {
            className: 'col-md-4 col-sm-4 col-xs-4',
            widgets: [{ key: 'CallsByYear' }],
          }, {
            className: 'col-md-4 col-sm-4 col-xs-4',
            widgets: [{ key: 'CasesByStateThisMonth' }],
          }],
        }],
      },
    };
  }

  render() {
    const {
        logout,
        userSPI,
    } = this.props;

    return (
      <div>
        <Navbar fluid>
          <div style={{ float: 'left', marginTop: '3px' }}>
            <DropdownButton
              id="actions"
              bsSize="large"
              bsStyle="link"
              title={<Glyphicon glyph="menu-hamburger" />}
              noCaret
            >
              {userCan(userSPI, 'view call records') ? <LinkContainer to="/"><MenuItem><FormattedMessage {...messages['action.app']} /></MenuItem></LinkContainer> : null}
              {userCan(userSPI, 'view data analytics') ? <MenuItem href="/analytics" target="_self"><FormattedMessage {...messages['action.analytics']} /></MenuItem> : null}
              {userCan(userSPI, 'manage users') ? <MenuItem href="/settings" target="_self"><FormattedMessage {...messages['action.admin']} /></MenuItem> : null}
              <MenuItem divider />
              <MenuItem onClick={logout}><FormattedMessage {...messages['action.signout']} /></MenuItem>
            </DropdownButton>
          </div>
          <Navbar.Header>
            <Navbar.Brand>
                Operations Dashboard for WAPIC
              </Navbar.Brand>
          </Navbar.Header>
        </Navbar>
        <Grid fluid>
          <Dashboard
            layout={this.state.layout}
            widgets={this.state.widgets}
            editable={false}
          />
        </Grid>
      </div>
    );
  }
}
DashboardPage.propTypes = {
  userSPI: PropTypes.object,
  logout: PropTypes.func,
};

const makeMapStateToProps = () => {
  const selectSPI = makeSelectSPI();
  const mapStateToProps = (state) => ({
    userSPI: selectSPI(state),
  });
  return mapStateToProps;
};

const mapDispatchToProps = (dispatch) => ({
  logout: () => {
    dispatch(logoutAction());
  },
});

export default connect(makeMapStateToProps, mapDispatchToProps)(DashboardPage);
