/* eslint-disable no-underscore-dangle */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { selectVars } from 'containers/App/selectors';
import { FormattedMessage } from 'react-intl';

import Alert from 'react-bootstrap/lib/Alert';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import Well from 'react-bootstrap/lib/Well';
import PageHeader from 'react-bootstrap/lib/PageHeader';
import Panel from 'react-bootstrap/lib/Panel';

import { FieldGroup, checkForUpdates } from 'utils/helpers';

import { loginRequest } from 'containers/App/actions';

import messages from './messages';

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
    };
  }

  componentWillMount() {
    checkForUpdates();
  }

  render() {
    const { data, dispatch, ...props } = this.props;
    const { error } = data;

    return (
      <div {...props}>
        <Well className="PageHeading">
          <PageHeader>
            <FormattedMessage {...messages.title} />
          </PageHeader>
        </Well>
        <Grid>
          <Row className="PageContent">
            <Col xs={12} sm={6} md={4} lg={4} mdOffset={1} lgOffset={1}>
              <Panel bsSize="lg" bsStyle={error ? 'danger' : 'default'}>
                <Panel.Heading>
                  <h3>Login</h3>
                </Panel.Heading>
                <Panel.Body>

                  { error ? <Alert bsStyle="danger">{error}</Alert> : null }

                  <form
                    onSubmit={(e) => {
                      e.preventDefault();
                      const { username, password } = this.state;
                      dispatch(loginRequest({ username, password }));
                    }}
                  >
                    <FieldGroup
                      id="username"
                      type="text"
                      label="Username"
                      onChange={(e) => this.setState({ username: e.target.value })}
                    />

                    <FieldGroup
                      id="password"
                      type="password"
                      label="Password"
                      onChange={(e) => this.setState({ password: e.target.value })}
                    />
                    <Button type="submit" bsStyle="primary">
                        Submit
                      </Button>
                  </form>
                </Panel.Body>
              </Panel>
            </Col>
            <Col xs={12} sm={6} md={4} lg={5} mdOffset={2}>
              <Panel>
                <Panel.Heading>
                  <h3>News</h3>
                </Panel.Heading>
                <Panel.Body>
                  <p><b>Demo Site</b><br />This is the demo site for the Global Poisons Information Centre System</p>
                </Panel.Body>
              </Panel>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

Login.propTypes = {
  data: PropTypes.object,
  dispatch: PropTypes.func,
};

function select(state) {
  return {
    data: selectVars(state),
  };
}

export default connect(select)(styled(Login)`
  .well {
    .page-header {
      border-bottom: none;
      h1 {
        font-weight: bold;
        text-align: center;
      }
    }
  }
  ${({ theme }) => theme.Login}
`);
