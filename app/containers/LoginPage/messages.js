import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'pic.containers.LoginPage.title',
    defaultMessage: 'Welcome to the Global Poisons Information Centre',
  },
});
