import { combineReducers } from 'redux';

import organisationReducer from './organisation';

import {
  UPDATE_INTERLOCUTOR_CATEGORY,
  UPDATE_INTERLOCUTOR_CONTACT_DETAIL,
  UPDATE_INTERLOCUTOR_NAME,
} from '../constants';

const category = (state = '', action) => {
  switch (action.type) {
    case UPDATE_INTERLOCUTOR_CATEGORY:
      return action.category;
    default:
      return state;
  }
};

const contact = (state = '', action) => {
  switch (action.type) {
    case UPDATE_INTERLOCUTOR_CONTACT_DETAIL:
      return action.contact;
    default:
      return state;
  }
};

const name = (state = '', action) => {
  switch (action.type) {
    case UPDATE_INTERLOCUTOR_NAME:
      return action.name;
    default:
      return state;
  }
};

const interlocutorReducer = combineReducers({
  category,
  contact,
  name,
  organisation: organisationReducer,
});
export default interlocutorReducer;
