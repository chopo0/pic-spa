import { combineReducers } from 'redux';

import {
  UPDATE_INTERLOCUTOR_ADDRESS1,
  UPDATE_INTERLOCUTOR_ADDRESS2,
  UPDATE_INTERLOCUTOR_CITY,
  UPDATE_INTERLOCUTOR_COUNTRY,
  UPDATE_INTERLOCUTOR_GEOMETRY,
  UPDATE_INTERLOCUTOR_LOCATION,
  UPDATE_INTERLOCUTOR_ORGANISATION,
  UPDATE_INTERLOCUTOR_POSTCODE,
  UPDATE_INTERLOCUTOR_STATE,
  UPDATE_INTERLOCUTOR_TELEPHONE,
} from '../constants';

const address1 = (state = '', action) => {
  switch (action.type) {
    case UPDATE_INTERLOCUTOR_ADDRESS1:
      return action.address1;
    default:
      return state;
  }
};

const address2 = (state = '', action) => {
  switch (action.type) {
    case UPDATE_INTERLOCUTOR_ADDRESS2:
      return action.address2;
    default:
      return state;
  }
};

const city = (state = '', action) => {
  switch (action.type) {
    case UPDATE_INTERLOCUTOR_CITY:
      return action.city;
    default:
      return state;
  }
};

const country = (state = '', action) => {
  switch (action.type) {
    case UPDATE_INTERLOCUTOR_COUNTRY:
      return action.country;
    default:
      return state;
  }
};

const geometry = (state = {}, action) => {
  switch (action.type) {
    case UPDATE_INTERLOCUTOR_GEOMETRY:
      return action.geometry ? { ...action.geometry } : {};
    default:
      return state;
  }
};

const location = (state = '', action) => {
  switch (action.type) {
    case UPDATE_INTERLOCUTOR_LOCATION:
      return action.location;
    default:
      return state;
  }
};

const name = (state = '', action) => {
  switch (action.type) {
    case UPDATE_INTERLOCUTOR_ORGANISATION:
      return action.organisation;
    default:
      return state;
  }
};

const postcode = (state = '', action) => {
  switch (action.type) {
    case UPDATE_INTERLOCUTOR_POSTCODE:
      return action.postcode;
    default:
      return state;
  }
};

const stateReducer = (state = '', action) => {
  switch (action.type) {
    case UPDATE_INTERLOCUTOR_STATE:
      return action.state;
    default:
      return state;
  }
};

const telephone = (state = '', action) => {
  switch (action.type) {
    case UPDATE_INTERLOCUTOR_TELEPHONE:
      return action.telephone ? action.telephone : '';
    default:
      return state;
  }
};

const organisationReducer = combineReducers({
  address1,
  address2,
  city,
  country,
  geometry,
  location,
  name,
  postcode,
  state: stateReducer,
  telephone,
});
export default organisationReducer;
