import { combineReducers } from 'redux';

import {
  UPDATE_INTERLOCUTOR_MEDIUM,
  UPDATE_INTERLOCUTOR_MEDIUM_DETAIL,
} from '../constants';

const detail = (state = '', action) => {
  switch (action.type) {
    case UPDATE_INTERLOCUTOR_MEDIUM_DETAIL:
      return action.detail ? action.detail : '';
    default:
      return state;
  }
};

const type = (state = 'Phone', action) => {
  switch (action.type) {
    case UPDATE_INTERLOCUTOR_MEDIUM:
      return action.medium;
    default:
      return state;
  }
};

const mediumReducer = combineReducers({
  detail,
  type,
});
export default mediumReducer;
