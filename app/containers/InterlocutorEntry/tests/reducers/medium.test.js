/* eslint-disable */

import mediumReducer from '../../reducers/medium';

import {
  updateInterlocutorMedium,
  updateInterlocutorMediumDetail,
} from '../../actions';

describe('mediumReducer', () => {
  it('returns the initial state', () => {
    expect(mediumReducer(undefined, {})).toMatchSnapshot();
  });
  it('update caller medium', () => {
    expect(mediumReducer({}, updateInterlocutorMedium(1, 'Email'))).toMatchSnapshot();
  });
  it('update caller medium detail', () => {
    expect(mediumReducer({}, updateInterlocutorMediumDetail(1, 'test@testing.test'))).toMatchSnapshot();
  });
});
