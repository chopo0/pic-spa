/* eslint-disable */

import organisationReducer from '../../reducers/organisation';

import {
  updateInterlocutorAddress1,
  updateInterlocutorAddress2,
  updateInterlocutorCity,
  updateInterlocutorCountry,
  updateInterlocutorGeometry,
  updateInterlocutorLocation,
  updateInterlocutorOrganisation,
  updateInterlocutorPostcode,
  updateInterlocutorState,
  updateInterlocutorTelephone,
} from '../../actions';

describe('organisationReducer', () => {
  it('returns the initial state', () => {
    expect(organisationReducer(undefined, {})).toMatchSnapshot();
  });
  it('update caller address 1', () => {
    expect(organisationReducer({}, updateInterlocutorAddress1(1, 'Address line 1'))).toMatchSnapshot();
  });
  it('update caller address 2', () => {
    expect(organisationReducer({}, updateInterlocutorAddress2(1, 'Address line 2'))).toMatchSnapshot();
  });
  it('update caller city', () => {
    expect(organisationReducer({}, updateInterlocutorCity(1, 'Cairo'))).toMatchSnapshot();
  });
  it('update caller country', () => {
    expect(organisationReducer({}, updateInterlocutorCountry(1, 'Slovenia'))).toMatchSnapshot();
  });
  it('update caller geometry', () => {
    expect(organisationReducer({}, updateInterlocutorGeometry(1, {'lat': 1, 'lng': -1}))).toMatchSnapshot();
  });
  it('update caller location', () => {
    expect(organisationReducer({}, updateInterlocutorLocation(1, 'Old location', 'Test location'))).toMatchSnapshot();
  });
  it('update caller organisation name', () => {
    expect(organisationReducer({}, updateInterlocutorOrganisation(1, 'Test organisation'))).toMatchSnapshot();
  });
  it('update caller postcode', () => {
    expect(organisationReducer({}, updateInterlocutorPostcode(1, '5000'))).toMatchSnapshot();
  });
  it('update caller state', () => {
    expect(organisationReducer({}, updateInterlocutorState(1, 'Devonshire'))).toMatchSnapshot();
  });
  it('update caller telephone', () => {
    expect(organisationReducer({}, updateInterlocutorTelephone(1, '(02) 9000 0000'))).toMatchSnapshot();
  });
});
